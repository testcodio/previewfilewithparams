var imageCode = 1;
var placeText = "in a castle";
var color = "";
var gColorArray = [];

// var gUserCode = "";
// var gCollectErrors = false;

function startGameEvent() {
  canvas = document.getElementById('easelCanvas');
}

window.readyEvents.addEventListener('ready', function() {
  $('#bg-value').fancySelect();
  gColorArray = [0, 0, 0];
  switch($.urlParam('id')) {
    case '5':
      resultVariablesList = [];
      resultVariablesList.push("gColorArray");
      break;
  }    
});

function switchBackground() {
  switch(imageCode) {
    case 1:
      document.getElementById("bg").setAttribute("src", "princess/img/castle.jpg");
      document.getElementById("princess").style.top = "140px";
      document.getElementById("princess").style.left = "271px";
      break;
    case 2:
      document.getElementById("bg").setAttribute("src", "princess/img/mountain.jpg");
      document.getElementById("princess").style.top = "154px";
      document.getElementById("princess").style.left = "209px";
      break;
    case 3:
      document.getElementById("bg").setAttribute("src", "princess/img/gingerbread.jpg");
      document.getElementById("princess").style.top = "154px";
      document.getElementById("princess").style.left = "361px";
      break;
  }
  document.getElementById("text").innerHTML = "Once upon a time there lived a beautiful princess <b>" + placeText + ".</b>";

}

function changeColor(color) {
  document.getElementById("text").style.backgroundColor = color;
  switch($.urlParam('id')) {
    case '5':
      gColorArray[imageCode-1] = color;   
      sendResults();
      break;
  }
}

function changeBGVar(e) {
  var option = document.getElementById("bg-value").value
  imageCode = parseInt(option.substr(0, 1));
  switch(imageCode) {
    case 1:
      placeText = "in a castle";
      break;
    case 2:
      placeText = "beside a snowy mountain";
      break;
    case 3:
      placeText = "in a gingerbread house";
      break;
  }
  var url = window.location.pathname;
  switch($.urlParam('id')) {
    case '2':
      runCodeEvent();
      switchBackground();
      break;
    case '3':
    case '4':
    case '5':
      loadUserScript();
      switchBackground();
      break;
  }
}

try {
  document.getElementById("bg-value").addEventListener(function(e) {
    imageCode = e.target.value;
  });
} catch(err) {

}