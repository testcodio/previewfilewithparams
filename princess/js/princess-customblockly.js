

// BACKGROUND CONDITION
// https://blockly-demo.appspot.com/static/demos/blockfactory/index.html#q5g5pj
// 
Blockly.Blocks['bg_condition'] = {
  init: function() {
    this.setHelpUrl('http://www.example.com/');
    this.setColour(225);
    this.appendDummyInput()
      .appendField("Background")
      .appendField("=")
      .appendField(new Blockly.FieldDropdown([["1", "1"], ["2", "2"], ["3", "3"]]), "NAME")
      .appendField("");
    this.setOutput(true);
    this.setTooltip('');
  }
};

Blockly.JavaScript['bg_condition'] = function(block) {
  var dropdown_name = block.getFieldValue('NAME');
  switch(dropdown_name) {
    case '1':
      var code = 'imageCode == 1';
      break;
    case '2':
      var code = 'imageCode == 2';
      break;
    case '3':
      var code = 'imageCode == 3';
      break;

  }
  var order = Blockly.JavaScript.ORDER_EQUALITY;
  return [code, order];
};  



// CHANGE BACKGROUND
Blockly.Blocks['bg_switch'] = {
  init: function() {
    this.setHelpUrl('http://www.example.com/');
    this.setColour(330);
    this.appendDummyInput()
      .appendField("Change Story");
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip('');
  }
};

Blockly.JavaScript['bg_switch'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = 'switchBackground();\n';
  return code;
};

// BACKGROUND VARIABLE
// https://blockly-demo.appspot.com/static/demos/blockfactory/index.html#ex5ink

Blockly.Blocks['background_var'] = {
  init: function() {
    this.setHelpUrl('http://www.example.com/');
    this.setColour(330);
    this.appendDummyInput()
      .appendField("var Background =")
      .appendField(new Blockly.FieldDropdown([["1", "1"], ["2", "2"], ["3", "3"]]), "OPTIONS");
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip('');
  }
};

Blockly.JavaScript['background_var'] = function(block) {
  var dropdown_options = block.getFieldValue('OPTIONS');
  // TODO: Assemble JavaScript into code variable.
  var code = 'imageCode = ' + dropdown_options + ';\n';
  return code;
};

Blockly.Blocks['text_var'] = {
  init: function() {
    this.setHelpUrl('http://www.example.com/');
    this.setColour(330);
    this.appendDummyInput()
      .appendField("var Place =")
      .appendField(new Blockly.FieldTextInput("in a castle"), "INPUT_TXT");
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip('');
  }
};


Blockly.JavaScript['text_var'] = function(block) {
  var text_input_txt = block.getFieldValue('INPUT_TXT');
  var code = 'placeText = \'' + text_input_txt + '\';\n';
  return code;
};  

// COLOR
Blockly.Blocks['set_color'] = {
  init: function() {
    this.setHelpUrl('http://www.example.com/');
    this.setColour(330);
    this.appendDummyInput()
      .appendField("Color")
      .appendField(new Blockly.FieldColour("#ff0000"), "NAME");
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip('');
  }
};  

Blockly.JavaScript['set_color'] = function(block) {
  var colour_name = block.getFieldValue('NAME');
  var code = 'changeColor(\'' + colour_name + '\');\n';
  return code;
};



   