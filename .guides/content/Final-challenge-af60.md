|||challenge
In this last challenge, we have some broken code that does not run, and should. There are 2 mistakes in there.

We've made it quite tricky to find out what it wrong, but it is nothing we have not already explained.

Here is a reminder of all the things you need to check for.

- Proper use of `( )` around the condition you are testing for.Proper
- Proper use of `{ }` around the code blocks for each code block that comes after the `if`, `else if` and `else`
- making sure conditions use `==` not `=`.

Off you go and once you're done, press the Check It button below.

{Check It}()

|||