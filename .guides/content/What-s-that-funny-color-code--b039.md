You'll notice in the code that we have something like

```javascript
changeColor('#ff0000');
```

We'll explain what this code means in another module. It is a so-called *hex code*. 

You can actually play around with it. Move your mouse over the code and you'll notice that a color box appears. Move your mouse into the box and then click. Feel free to change the color in the popup window that appears.

