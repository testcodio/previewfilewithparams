Take a look at the code again.

```javascript
if (imageCode == 1) {
```

You may be wondering why we use `==` and not just `=`.

The reason is that if you put `imageCode = 1`, Javascript would assign a value of `1` to `imageCode` rather than testing whether `imageCode` equals 1. 

It would carry out the instruction but  always return `true`.

So, just remember that when using an `if` condition, use `==`. It's easy to forget and it can be surprisingly hard to find.

