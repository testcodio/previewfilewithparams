Let's get straight to coding the following scenario.

- If the `Background` variable is 3 (the gingerbread house), then change the color of the text background to another color.

Now let's create some code.

1. Drag in the 'If/Do' block from the toolbox. 
1. Grab the 'Background=1' block with the bit sticking out on the left and plug it into the 'If/Do' block.
1. Change it to Background = 3. What we now have is the "if the background code is equal to 3" part.
1. Now grab a 'Color' block and plug it into the 'do' socket below the if part.
1. You can change the color by clicking on the color box.

It should look like this ...

![](.guides/img/if-block.png)

## Play with it
To really get the feel for what is happening, click on the drop down box above the picture. As soon as you change it to 3, the color of the text background will change.