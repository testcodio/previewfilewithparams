So we now have the full if/else if/else code on the left hand side. Take a look.

You'll see that the way we work with the condition for the `else if` statement is that same as we explained for the `if` condition.

