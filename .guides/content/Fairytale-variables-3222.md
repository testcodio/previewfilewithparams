Let's do a bit more with variables first to get used to our new scenario. 

Over on the left hand side, you can see the first part of a charming fairytale. 

When we get to real programming, we often use numbers as a code to represent things. For example

- 1 is the code for 'Castle'.
- 2 is the code for 'Mountain'.
- 3 is the code for 'Gingerbread House'.

Let's do something.

1. Drag the **var Background** block into the Code area. This sets the Background variable to 1, 2 or 3.
1. Change its value to 2.
1. Drag the **var Place** variable in. Change this text to be anything you like.
1. Drag the **Change Story** command in,
1. Press the 'Run it' button and see what happens.
1. Change the Background or Place variables that you just added and run it again.

Notice how the background image and the end of text changes.

Remember, each time you change the Background code or the Place text, you are changing a variable.

When you make changes to a variable, press the 'Run it' button to see the changes.

## Color
We've also got a variable for Color. Try playing around with that, too.

