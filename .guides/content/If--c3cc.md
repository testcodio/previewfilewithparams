We are now going to introduce a brand new and incredibly important topic. The proper terminology for what we are about to discuss is **Conditional Statements** but let's put that to the back of our minds for now.

|||definition
A conditional statement says that "if some **condition** is true, then and only then execute the following statement or statements". In our example

- If the `Background` variable is `Mountain`, then change the color of the text background to blue.
- If the `Background` variable is `Gingerbread`, then change the color of the text background to green.
- etc.

|||

On the next page, we'll see exactly how to do this.
