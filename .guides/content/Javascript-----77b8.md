Right, we're now going to see how the same stuff works using Javascript.

You'll see a lot of new symbols here, especially the `{` and `}`. We'll explain what these are and how to get comfortable with them. They actually make sense, so please stick with it.