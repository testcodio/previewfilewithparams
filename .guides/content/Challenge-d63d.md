|||challenge
Create some code that does the following

1. Set the Background code for the Gingerbread house.
1. Set the Place text to be " in a tasty house" (no spaces at the start or end of this please).
1. Run the Change Story command.
1. Press the Run It! button and check the output.

Once you're done, press the "Check It" button below to see if you got it right.

{Check It}(node /home/codio/workspace/.guides/tests/game-tests.js fairytale01)

|||