One thing you'll notice is that once you start playing with the dropdown box and the color changes, it never changes back again. Let's sort that out.

Click on the cog icon in the 'If/Do' block and you will see a window pop out.

![](.guides/img/else-popout.png)

If it is partially out of sight, you can drag its border into view.

Drag the 'else' block into the 'if' section (see the arrow in the image). Once it is snapped in place, you can close it again by clicking on the cog icon again.

Now, drag a color block into the newly arrived 'else' socket within your code. It should look something like this.

![](.guides/img/else-done.png)

Press the 'Run it' button and then start playing with the dropdown list again.

## What's it doing?
What this is now doing is the following

> If the `Background` variable is 3 (the gingerbread house), then change the color of the text background to red.
>
> Else (which means 'otherwise'), set it to green

