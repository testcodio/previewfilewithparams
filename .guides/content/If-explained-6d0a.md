## What is a condition?

|||definition
A condition is something you look at and ask yourself the question "is this *true* or *false*.

|||

Remember our variables discussion? `imageCode` is our variable name and it stores the image code, so `1` (castle), `2` (mountain) or `3` (gingerbread house) in our example.

So `if (imageCode == 3)` is asking the question "is the image code equal to 3"?

- if it does equal 3, then the condition is **true**.
- if it does not equal 3 (in other words it is 1 or 2) then the condition is **false**.

## If the condition is true
If the condition is true, then the instructions that come within the `{ }` will be executed.

If the condition is false, then code within the `{ }` will **not** execute.

It's as simple as that.

Now take a really good look and make sure you understand this. 

## The brackets matter
You'll notice that conditions are contained with parentheses (brackets). This is important and you cannot leave them out.




