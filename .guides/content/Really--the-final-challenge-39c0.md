Just to reinforce your knowledge about conditional statements and variables, here is a final challenge.

|||challenge
Write some code that does the following

1. Like as on the previous page, you should test for each of the 3 `imageCode` conditions using `if`, `else if` and `else`.
2. As well as changing the color, you now have to change the place text. On the previous page, the text was changed for you. Now you have to do it.
3. You do this by using the `placeText` variable and assigning it whatever text you want within each of the conditional code blocks `{ }`.

Test it out by using the drop down box and when you think you've got it working, press the Check It button below.
|||