And here comes the final part. The problem we have now is that both the mountain and the gingerbread house get the same background text color.

We want to make sure each one gets its own color. 


|||challenge
Using the same cog icon, create and else if statement **immediately after** the if (before the else if) and drag a color in there.

Have a play with the dropdown box and make sure that when you change it to any of the 3 backgrounds, you get a different color.

If you've got it right, you should now see a different color appear each time you change the drop down list selection.
|||

## What's it doing?
</br>
> So now what we're saying is ...
>
> If the `Background` variable is 3 (the gingerbread house), then change the color of the text background to red.
>
> Else if (which means 'otherwise, if') the Background variable is 2 (the mountain) then set it to whatever color you specified
>
> Else (which means 'otherwise'), set it to the final color.

Really make sure you understand what is going on here and you will have one of programming's important foundational concepts.

