
|||challenge
You'll now see a piece of code with 3 `if` statements. Each of them is not correctly formatted.

Your job is to fix the code so it works properly. Each one should have the correct `()` and `{}` arrangement we discussed 2 pages back.

After changing your code, click on the reload preview button and check you are not getting any syntax errors.

When you think you've got it right, click on the Check It button below and we'll check for you.

If you've got it right, you should get a different color for each selection you make in the dropdown.

{Check It}(node /home/codio/workspace/.guides/tests/game-tests.js user-02)
|||

