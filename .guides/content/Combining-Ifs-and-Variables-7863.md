We are going to ask you to perform a challenge.

|||challenge
On the left. you'll see some code. The `changeColor(  )` parentheses `(  )` are empty, however.

What we want you to do is to use variables to change the color rather than the color codes.

You should be able to see that the color values are already assigned to the variables in the first statements. All you need to do is insert these variable names with in the `(  )` so it works properly.

When you have made you changes, press the reload preview button. Once it works as you expect, press the Check It button below.

{Check It}(node /home/codio/workspace/.guides/tests/game-tests.js user-04)

|||