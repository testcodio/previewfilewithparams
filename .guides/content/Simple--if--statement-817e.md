We'll start of by handling the simple `if` statement. If you remember our code blocks, it looked like this.

![](.guides/img/simple-if.png)

Look over on the left hand side and you'll see the Javascript equivalent.

On the next page, we'll explain what is going on.
