var Hapi = require('hapi');

var gResult;

// Create a server with a host and port
var server = new Hapi.Server();
server.connection({ 
    host: '0.0.0.0', 
    port: 9500,
    routes: {
      cors: {
        origin: ['*']
      }
    }   
});

// Receive test data from the game
server.route({
    method: 'POST',
    path:'/test-data', 
    handler: function (request, reply) {
       gResult = request.payload;       
       console.log("GOT TEST DATA : " + request.payload);
       reply('ok');
    }
});

// Run the test
server.route({
    method: 'GET',
    path:'/test-result', 
    handler: function (request, reply) {
       console.log("Request for /test-result");
       reply(gResult);
    }
});


// Start the server
server.start();