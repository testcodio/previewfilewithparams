var request = require('request');


function fairytale01(res) {
  if(res.imageCode == 3 && res.placeText == 'in a tasty house') {
    console.log("Well done!!");
    process.exit(0);   
  }
  else {
    console.log("You haven't set the variables correctly yet. You set the image code to be '" + res.imageCode + "' and the place text variable to be '" + res.placeText + "'.");  
    process.exit(1);      
  }  
}


function user02(res) {
  if(res.gScriptResult == 0) {
    console.log("Well done!!");
    process.exit(0);   
  }
  else {
    console.log("There are still errors on this page! Try again.");  
    process.exit(1);      
  }  
}

function user04(res) {
  if(res.gColorArray[0] == null || res.gColorArray[0] == 0 ||
     res.gColorArray[1] == null || res.gColorArray[1] == 0 ||
     res.gColorArray[2] == null || res.gColorArray[2] == 0) {
        console.log("You've not yet got this right. Make sure you run through each of the drop-down list selections first.");
        process.exit(1);      
  }
  else {
    console.log("Well done!!");
    process.exit(0);      
  }  
}

var domain = process.env.CODIO_BOX_DOMAIN;
request("https://" + domain + ":9500/test-result", function(error, response, body) {

  // Check for a system error such as Connection Refused
  if(error && response==undefined) {    
    console.log(error);  
    process.exit(2);
  }
  
  if (!error && response.statusCode == 200) {
    
    // HAPI Server returned OK
    if(body == '') {
      // No data returned by game, so tell user
      console.log("Make sure you press the Run button before you check your code.");  
      process.exit(1);            
    }

    // Call the actual data test
    res = JSON.parse(body);
    switch(process.argv[2]) {
      case 'rocket01':
        rocket01(res);
        break;
      case 'fairytale01':
        fairytale01(res);
        break;
      case 'user-02':
        user02(res);
        break;  
      case 'user-04':
        user04(res);
        break;          
    }
    
  }
  else {
    
    // Some other error, so report
    if (response.statusCode == 502) {
      console.log("[" + response.statusCode + "] The service is not started. Please exit your project and re-open it. If you still see this error, please contact support@codio.com.")
    }  
    process.exit(2);
    
  }
  
});