$.urlParam = function(name){
  var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
  if (results==null){
     return null;
  }
  else{
     return results[1] || 0;
  }
}

var resultVariablesList = [];
var gCollectErrors = false;
var gUserCode="";
var gScriptResult=1;
var useBlockly = $.urlParam('blockly');
var gGamePath = $.urlParam('tutorial');
var gId = $.urlParam('id');

window.showCodeButtonEvents = new EventDispatcher();
window.runCodeButtonPreEvents = new EventDispatcher();
window.runCodeButtonEvents = new EventDispatcher();
window.readyEvents = new EventDispatcher();

function showCodeButtonClick() {
  window.showCodeButtonEvents.dispatchEvent({
    type: 'click'
  });
}

function runCodeButtonClick() {
  window.runCodeButtonPreEvents.dispatchEvent({
    type: 'click'
  });
  window.runCodeButtonEvents.dispatchEvent({
    type: 'click'
  });
}

function updateToolboxLabel(value) {
  document.getElementById("toolboxLabel").style.width = value + "px";
  document.getElementById("instructionsLabel").style.width = "calc(100% - " + value + "px)";
}

function initBlockly(tutorialXML) {
  if (!useBlockly) return;
  var blocklyArea = document.getElementById("blockly-area");
  var startBlocks = tutorialXML.getElementsByTagName('startBlocks')[0];
  var toolboxLabelSize = tutorialXML.getElementsByTagName('toolboxLabelSize')[0].firstChild.data;
  var toolbox = tutorialXML.getElementsByTagName('toolbox')[0];
  var resultVariablesXMLElement = tutorialXML.getElementsByTagName('resultVariables');
  readResultVariablesFromXML(resultVariablesXMLElement);
  toolbox = toolboxLabelSize !== 0 ? toolbox : '';
  Blockly.inject(blocklyArea, {toolbox: toolbox});
  Blockly.Xml.domToWorkspace(Blockly.mainWorkspace, startBlocks);
  updateToolboxLabel(toolboxLabelSize);
}

function readResultVariablesFromXML(resultVariablesXMLElement) {
  if (resultVariablesXMLElement.length > 0) {
    for (var i = 0; i < resultVariablesXMLElement[0].children.length; i++) {
        var varName = resultVariablesXMLElement[0].children[i].textContent;
        resultVariablesList.push(varName);
    }
  }
}

function loadUserScript() {
  var scriptFile = $.urlParam('script');
  if(scriptFile != undefined) {
    gCollectErrors = true;
    gScriptResult = 0;
    resultVariablesList.push("gScriptResult");    
    $.ajax({
      async: true,
      type: 'GET',
      url: "https://" + new URL(window.location.href).hostname + "/" + gGamePath + "/" + scriptFile,
      data: gUserCode,
      success: function(data) {
        window.onerror = function(error, url, line) {
          if (gCollectErrors) {
            gScriptResult = 1;
            sendResults();
            gCollectErrors=false;
            sweetAlert({title: "<small>Syntax Error</small>!",
              text: "<b>Line:</b> " + line + "</br><b>Error:</b> " + error,
              type: "warning",
              html: true });    
          }
          window.onerror = null;          
        }       
        eval(data);
        sendResults();
      },
      complete: function(jqXHR, error) {
        if(jqXHR.status == 404) {
          sweetAlert("Error", "Problem fetching the script. Contact support@codio.com.");
          if(gCollectErrors) {
            gScriptResult = 1;
            sendResults();
            gCollectErrors = false;
            sweetAlert({
              title: "<small>Syntax Error</small>!",
              text: "<b>Line:</b> " + line + "</br><b>Error:</b> " + error,
              type: "warning",
              html: true
            });
          }
          gCollectErrors = false;
        }
      },
      dataType: 'text'
    });
  }
}

function sendResults() {/*
  var obj = {};
  resultVariablesList.forEach(function (varName) {
    obj[varName] = this[varName];
  });
  xmlHttp = new XMLHttpRequest();
  xmlHttp.open( "POST", "https://" + new URL(window.location.href).hostname + ":9500/test-data", false);
  xmlHttp.send(JSON.stringify(obj));
  if( xmlHttp.responseText != "ok") {
    swal("Problem sending in the test results!");
  }*/
}

function loadXMLDoc(filename) {
  if (window.XMLHttpRequest) {
    xhttp=new XMLHttpRequest();
  }
  else {
    xhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xhttp.open("GET",filename,false);
  xhttp.send();
  return xhttp.responseXML;
}

function showCodeEvent() {
  Blockly.JavaScript.INFINITE_LOOP_TRAP = null;
  var code = Blockly.JavaScript.workspaceToCode();
  code = code.split('\n').join('<br>');
  swal({
    title: "JavaScript Code",
    text: "<div style='text-align: left; display: block;'>"+code+"</div>",
    html: true
  });
};

function runCodeEvent() {
  window.LoopTrap = 1000;
  Blockly.JavaScript.INFINITE_LOOP_TRAP =
    'if (--window.LoopTrap == 0) throw "Infinite loop.";\n';
  var code = Blockly.JavaScript.workspaceToCode();
  Blockly.JavaScript.INFINITE_LOOP_TRAP = null;
  try {
    eval(code);
    if (resultVariablesList.length > 0) {
      sendResults();
    }
  }
  catch (e) {
    swal("Error", e.message);
  }
};

$(document).ready(function() {
  $.getScript(gGamePath + '/tutorialdef.js', function(data, textStatus, jqxhr) {
    var tutorial = tutorialDef[gId];
    var tutorialXML = loadXMLDoc(tutorial.xml);
    $('#content').load(tutorial.html, function () {
      if (useBlockly) {
        var code = $("#codePartition");
        $("#codePartition").show();
        $("#codePartition").css("height", "50%");
        $("#contentPartition").css("height", "50%");
        initBlockly(tutorialXML);
      }
      loadUserScript();
      window.readyEvents.dispatchEvent({
        type: 'ready'
      });
    });
  });
});

window.showCodeButtonEvents.addEventListener('click', showCodeEvent);
window.runCodeButtonEvents.addEventListener('click', runCodeEvent);

